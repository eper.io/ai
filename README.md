# AI

This is a simple AI tutorial to teach AI to non-tech people ... PG13.

## Usage

`git clone https://gitlab.com/eper.io/ai.git`

`go test gitlab.com/eper.io/ai/ai`

## Contact and support

Do you have business?
Do you need servicing from us?

- support agreement
- warranty, and reliability guarantee
- patent licensing and third party patent licensing
- security certification
- legal and privacy compliance certificates
- integration (Kubernetes, Helm, etc.)

**Please contact miklos.szegedi@eper.io at Schmied Enterprises LLC.**