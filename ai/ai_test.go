package ai

import (
	"fmt"
	"strings"
	"testing"
	"time"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

func TestNumbers(t *testing.T) {
	if "123" != NumberPart("123") {
		t.Error("123")
	}
	if "-123" != NumberPart("-123") {
		t.Error("-123")
	}
	if "+123" != NumberPart("+123") {
		t.Error("+123")
	}
	if "NaN" != NumberPart("") {
		t.Error("<empty>")
	}
	if "123" != NumberPart("abc123") {
		t.Error("abc123")
	}
	x := Times("123", "123")
	if "15129" != x {
		t.Error("123*123")
	}
}

func TestForwardPropagate(t *testing.T) {
	sampleRun := Tensor(
		`
Node 0 is 100.
Node 1 is 50.
Node exp is 45.
Edge 0 to 2 is 50.
Edge 0 to 3 is 50.
Edge 1 to 3 is -10.
Edge 3 to res is 100.
Edge res to loss is 100.
Edge exp to loss is -100.
`)
	tensor, loss := epoch(sampleRun)
	if loss != "0" {
		t.Error(tensor)
	}
}

func TestForwardPropagateLoss(t *testing.T) {
	sampleRun := Tensor(
		`
Node 0 is 100.
Node 1 is 50.
Node exp is 48.
Edge 0 to 2 is 50.
Edge 0 to 3 is 50.
Edge 1 to 3 is -10.
Edge 3 to res is 100.
Edge res to loss is 100.
Edge exp to loss is -100.
`)
	tensor, loss := epoch(sampleRun)
	if Abs(loss) != "3" {
		t.Error(tensor)
	}
}

func TestXOR(t *testing.T) {
	model := Tensor(`
Edge 0 to h0 is 50.
Edge 1 to h0 is 50.
Edge max to h0 is 50.
Edge 0 to h1 is -50.
Edge 1 to h1 is -50.
Edge max to h1 is 150.
Edge h0 to h2 is 50.
Edge max to h2 is -25.
Edge h1 to h2 is 50.
Edge h2 to res is 134.
Edge max to res is 0.
Edge res to loss is 100.
Edge exp to loss is -100.
`)

	sample00 := Tensor("Node max is 100.\nNode 0 is 0.\nNode 1 is 0.\nNode exp is 0.\n")
	sample10 := Tensor("Node max is 100.\nNode 0 is 100.\nNode 1 is 0.\nNode exp is 100.\n")
	sample01 := Tensor("Node max is 100.\nNode 0 is 0.\nNode 1 is 100.\nNode exp is 100.\n")
	sample11 := Tensor("Node max is 100.\nNode 0 is 100.\nNode 1 is 100.\nNode exp is 0.\n")
	samples := []Tensor{sample00 + model, sample01 + model, sample10 + model, sample11 + model}

	for _, sample := range samples {
		tensor, _ := epoch(sample)
		fmt.Println(tensor)
		if !strings.Contains(string(tensor), "Node loss is 0.") {
			t.Error("Loss", tensor)
		}
	}
}

func TestXORTrainingDone(t *testing.T) {
	samples := newXORTrainingSet()
	_, loss := MonteCarloTraining(samples, "0.1", 1*time.Second)
	if loss != "0" {
		t.Error(loss)
	}
}

func TestXORTrainingCloseFit(t *testing.T) {
	samples := newXORTrainingSet()
	closeFit := monteCarloModifyModelAll(samples)
	betterFit, loss := MonteCarloTraining(closeFit, "-1", 5*time.Second)
	if loss != "0" {
		t.Error(loss)
	}
	fmt.Println(loss, closeFit, betterFit)
}

func newXORTrainingSet() []Tensor {
	model := Tensor(`
Edge 0 to h0 is 50.
Edge 1 to h0 is 50.
Edge max to h0 is 50.
Edge 0 to h1 is -50.
Edge 1 to h1 is -50.
Edge max to h1 is 150.
Edge h0 to h2 is 50.
Edge max to h2 is -25.
Edge h1 to h2 is 50.
Edge h2 to res is 134.
Edge max to res is 0.
Edge res to loss is 100.
Edge exp to loss is -100.
`)
	sample00 := Tensor("Node max is 100.\nNode 0 is 0.\nNode 1 is 0.\nNode exp is 0.\n")
	sample10 := Tensor("Node max is 100.\nNode 0 is 100.\nNode 1 is 0.\nNode exp is 100.\n")
	sample01 := Tensor("Node max is 100.\nNode 0 is 0.\nNode 1 is 100.\nNode exp is 100.\n")
	sample11 := Tensor("Node max is 100.\nNode 0 is 100.\nNode 1 is 100.\nNode exp is 0.\n")
	samples := []Tensor{sample00 + model, sample01 + model, sample10 + model, sample11 + model}
	for i, sample := range samples {
		samples[i] = Tensor(strings.ReplaceAll(string(sample), "\n\n", "\n"))
	}
	return samples
}
