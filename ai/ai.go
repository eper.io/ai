package ai

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

// This is an AAA Affordable AI Agent module.
// It can handle small networks easily.
// It is a good starter to explain nonlinear networked logic
// It can be trained easily
// It is fairly cheap to ramp up being few hundred lines
// It is fairly cheap to debug up having plain text labeled model and node info
// It can be quick not forcing a sixteen to thirty two bit number scaled but a single byte, for booleans

const FromNode = 0
const ToNode = 1
const Weight = 2
const MaxLayers = 100

func epoch(sampleAndModel Tensor) (ret Tensor, loss Scalar) {
	layer := sampleAndModel
	loss = Scalar("0")
	for i := 0; i < MaxLayers; i++ {
		nodesBase, nodesDiff, model := ForwardPropagate(layer)
		applyDiffToNetwork(nodesBase, nodesDiff)
		b := bytes.Buffer{}
		for k, v := range nodesBase {
			Steel(b.WriteString(fmt.Sprintf("Node %s is %s."+"\n", k, v)))
			if k == "loss" {
				loss = Abs(Scalar(v))
			}
		}
		for _, edge := range model {
			b.WriteString(fmt.Sprintf("Edge %s to %s is %s."+"\n", edge[FromNode], edge[ToNode], edge[Weight]))
		}
		layer1 := Tensor(b.String())
		if layer1 == layer {
			break
		}
		layer = layer1
	}
	ret = layer
	return
}

func ForwardPropagate(sampleAndModel Tensor) (map[string]string, map[string]string, [][]string) {
	chNodes := make(chan map[string]string)
	go func(sample Tensor, done chan map[string]string) {
		scanner := bufio.NewScanner(bytes.NewBufferString(string(sample)))
		nodeIx := map[string]string{}
		for scanner.Scan() {
			sentence := scanner.Text()
			if sentence == "" {
				continue
			}
			sentence = TrimDot(sentence)
			var name, activation string
			Steel(fmt.Sscanf(sentence, "Node %s is %s", &name, &activation))
			if name != "" {
				nodeIx[name] = activation
			}
		}
		done <- nodeIx
	}(sampleAndModel, chNodes)

	che := make(chan [][]string)
	go func(sample Tensor, done chan [][]string) {
		scanner := bufio.NewScanner(bytes.NewBufferString(string(sample)))
		edges := make([][]string, 0)
		for scanner.Scan() {
			sentence := scanner.Text()
			if sentence == "" {
				continue
			}
			if sentence[len(sentence)-1] == '.' {
				sentence = sentence[:len(sentence)-1]
			}
			var from, to, weight string
			Steel(fmt.Sscanf(sentence, "Edge %s to %s is %s", &from, &to, &weight))
			if from != "" {
				edges = append(edges, []string{from, to, weight})
			}
		}
		done <- edges
	}(sampleAndModel, che)

	nodes := <-chNodes
	model := <-che
	diff := forwardPropagate(nodes, model)

	return nodes, diff, model
}

func applyDiffToNetwork(nodeIx map[string]string, diff map[string]string) {
	for k, v := range diff {
		nodeDiff := strings.Split(v, "+")
		var y int
		val := "0"
		for _, x := range nodeDiff {
			if x == "NaN" {
				val = "NaN"
				break
			}
			y = y + int(Decimal(Scalar(x)))
		}

		if val != "NaN" {
			y = y // + int(Decimal(nodeIx[k]))
			y = (y + 0) / 100
			val = string(decimalS(int16(Activate(y))))
		}
		orig, ok := nodeIx[k]
		if ok && orig != "NaN" && orig != val {
			// Changed in multiple epoch of different layers
			nodeIx[k] = "NaN"
			continue
		}
		nodeIx[k] = val
	}
}

func forwardPropagate(nodeIx map[string]string, rules [][]string) map[string]string {
	diff := map[string]string{}
	for _, edge := range rules {
		if len(edge) == 0 {
			continue
		}
		fromLabel := edge[0]
		toLabel := edge[1]
		weight := Scalar(edge[2])

		base := Scalar(nodeIx[fromLabel])
		if base != "NaN" {
			factor := Times(base, weight)
			diff[toLabel] = string(Nvl(Scalar(diff[toLabel]), "0")) + "+" + string(factor)
		}
	}
	return diff
}

// Activate is a default nonlinear activation function.
// It allows aligning to nonlinear models.
// It also has two caps to zero that can easily handle ranges.
// We avoid sigmoid due to the fact that
// it loses precision at high outputs causing "logic leaking"
// ~ random uncorrelated nodes.
func Activate(i int) int8 {
	if i < -100 {
		return 0
	}
	if i > 100 {
		return 0
	}
	return int8(i)
}
