package ai

import (
	"bufio"
	"bytes"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

// Problem
// There are many ways to train AI models.
// We just choose the most simple Monte Carlo method.
// The concept is similar to Miklos Szegedi's early paper Microeconomic and Macroeconomic simulation models in 2003.
// We use the simplest concept, basically an organic Bauhaus of Artificial Intelligence.
//
// Solution:
// This is good enough for learning.
// Other algorithms like random forest, k means etc. have only additional advantage.
// Anything they find would be found by Monte Carlo, except that it is slower.
// Training algorithms usually get a hint from the application area.
// This hint can be used by an A* or similar algorithm to
// narrow down to the best fit model faster.
// They save training time and cost.
//
// Problem: Such a Monte Carlo model will be useful for most non-linear problems
// Solution:
// Indeed. Monte Carlo will choose momentum, when there is only one weight to reach a level.
// It will then back propagate finding that the current node cannot be changed more.
// It will take more samples into consideration to weigh in changing constant or a weight.
// It will add a node, when the loss of the current model
// is not responsive to changes statistically.
//
// Problem: Can it be used for unsupervised learning?
// Solution:
// Yes. We intentionally include the expected output in the training samples.
// The loss function is part of the model pointing back to the sample.
// Unsupervised models can eventually match the loss
// with the increasing complexity of the model,
// and the decreasing complexity of a random set of output hidden layer nodes.

func MonteCarloTraining(trainingSet []Tensor, target Scalar, timeout time.Duration) (peak []Tensor, peakLoss Scalar) {
	peak = []Tensor(nil)
	peakLoss = Scalar("0")

	peak = trainingSet
	_, peakLoss = epochSet(peak)
	if Less(peakLoss, target) {
		return
	}

	start := time.Now()
	for {
		if time.Now().Sub(start).Microseconds() > timeout.Microseconds() {
			return
		}

		for i := 0; i < len(peak); i++ {
			// Modify the model randomly by changing weights
			// The bigger the proportion of edges the less likely we add more nodes.
			// A good fit model will add node, a worse fit will change weight
			candidate := monteCarloModifyModelAll(peak)
			candidate, loss := epochSet(candidate)
			//fmt.Println(loss, peakLoss)
			if Less(loss, Abs(peakLoss)) {
				peak = candidate
				peakLoss = loss
			}
		}
	}
}

func monteCarloModifyModelAll(candidate []Tensor) []Tensor {
	modelChanged := monteCarloModifyAModelWeight(candidate[0])

	ret := make([]Tensor, len(candidate))
	for i := 0; i < len(candidate); i++ {
		scanner := bufio.NewScanner(strings.NewReader(string(candidate[i])))
		out := bytes.Buffer{}
		for scanner.Scan() {
			line := scanner.Text()
			if !strings.HasPrefix(line, "Edge") {
				out.WriteString(line)
				out.WriteString("\n")
				continue
			}
		}
		out.WriteString(string(modelChanged))
		ret[i] = Tensor(out.String())
	}
	return ret
}

func epochSet(candidate []Tensor) ([]Tensor, Scalar) {
	ret := make([]Tensor, len(candidate))
	total := Scalar("0")
	for i := 0; i < len(candidate); i++ {
		current, loss := epoch(candidate[i])
		total = plus(total, loss)
		ret[i] = current
		//fmt.Println(current, loss)
		//fmt.Println()
	}
	return ret, total
}

func monteCarloModifyAModelWeight(model Tensor) Tensor {
	scanner := bufio.NewScanner(strings.NewReader(string(model)))
	var edges int
	var all int
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "Edge") {
			edges++
		}
		all++
	}

	out := bytes.Buffer{}
	rand.Seed(int64(time.Now().Nanosecond()))

	scanner = bufio.NewScanner(strings.NewReader(string(model)))
	var i int
	for scanner.Scan() {
		x := TrimDot(scanner.Text())
		var from, to, weight Scalar
		Steel(fmt.Sscanf(x, "Edge %s to %s is %s", &from, &to, &weight))
		if from != "" {
			sum, ok := modifyRandomEdge(i, edges, all, weight)
			if ok {
				x := fmt.Sprintf("Edge %s to %s is %s.", from, to, sum)
				out.WriteString(x)
				out.WriteString("\n")
				i++
				continue
			}
			sum, ok = extendRandomEdge(i, edges, all, weight)
			if ok {
				x := fmt.Sprintf("Edge %s to %s is %s.", from, to, sum)
				out.WriteString(x)
				out.WriteString("\n")
				i++
				continue
			}
		}
	}
	return Tensor(out.String())
}

func modifyRandomEdge(current int, modelChange int, all int, starting Scalar) (Scalar, bool) {
	hint := rand.Intn(all)
	if hint != current {
		return starting, false
	}
	if current < modelChange {
		diff := Scalar(fmt.Sprintf("%d", (rand.Int31()%20)-10))
		sum := plus(starting, diff)
		return sum, true
	}
	return starting, false
}

func extendRandomEdge(current int, modelChange int, all int, starting Scalar) (Scalar, bool) {
	// TODO add a node with nodes/nodes+edges probability
	return starting, false
}
