package ai

import (
	"fmt"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

var debug = false

type Tensor string
type Scalar string

func Equals(x []int8, y []int8) bool {
	for i := 0; i < len(x); i++ {
		if x[i] != y[i] {
			return false
		}
	}
	return true
}

// Steel handles streams as stable.
// Such streams will be handled by GPU, NPU, that does not do conditional error handling
func Steel(n int, e error) {
	if e != nil && debug {
		fmt.Println(e)
	}
}

func NumberPart(b Scalar) Scalar {
	bl := len(b)
	for i := 0; i < bl; i++ {
		if b[bl-i-1] >= '0' && b[bl-i-1] <= '9' {
			continue
		}
		if b[bl-i-1] == '+' || b[bl-i-1] == '-' {
			continue
		}
		if i > 0 {
			b = b[bl-i:]
			break
		}
		return "NaN"
	}
	if bl == 0 {
		return "NaN"
	}
	return b
}

func Times(a Scalar, b Scalar) Scalar {
	a = NumberPart(a)
	b = NumberPart(b)
	if a == "NaN" || b == "NaN" {
		return "NaN"
	}
	return decimalS(Decimal(b) * Decimal(a))
}

func plus(a Scalar, b Scalar) Scalar {
	a = NumberPart(a)
	b = NumberPart(b)
	if a == "NaN" || b == "NaN" {
		return "NaN"
	}
	return decimalS(Decimal(b) + Decimal(a))
}

func Less(a Scalar, b Scalar) bool {
	a = NumberPart(a)
	b = NumberPart(b)
	if a == "NaN" || b == "NaN" {
		return false
	}
	return Decimal(a) < Decimal(b)
}

func Abs(a Scalar) Scalar {
	a = NumberPart(a)
	if a == "NaN" {
		return "NaN"
	}
	if a[0] == '-' {
		return Scalar(a[1:])
	}
	return Scalar(a)
}

func Decimal(b Scalar) int16 {
	bl := len(b)
	var x int16
	d := int16(1)
	for j := 0; j < len(b); j++ {
		if b[bl-j-1] == '-' {
			return -x
		}
		x = x + int16(b[bl-j-1]-'0')*d
		d = d * 10
	}
	return x
}

func decimalS(si int16) Scalar {
	var s = []rune{'0', '0', '0', '0', '0'}
	i := si
	if si < 0 {
		i = -si
	}
	sl := len(s)
	for j := 0; i > 0; j++ {
		s[sl-j-1] = rune(i%10) + '0'
		i = i / 10
		if i == 0 {
			if si < 0 {
				return Scalar("-" + string(s[sl-j-1:]))
			}
			return Scalar(s[sl-j-1:])
		}
	}
	if i == 0 {
		return "0"
	}
	return Scalar("cccc")
}

func Nvl(s Scalar, default0 Scalar) Scalar {
	if s == "" || s == "NaN" {
		return default0
	}
	return s
}

func TrimDot(sentence string) string {
	if len(sentence) > 0 && sentence[len(sentence)-1] == '.' {
		sentence = sentence[:len(sentence)-1]
	}
	return sentence
}
